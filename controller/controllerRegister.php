<?php
require_once("../helper/validations.php");
if (isset($_POST['submit'])){
    session_start();
    $_SESSION['errors'] = array("hola");

    if (!ValidarNombre($_POST['nombre'])) {
        $_SESSION['errors'][] = "Nombre Invalido";
    }

    if (ValidarApellidos($_POST['apellidos'])){
        $_SESSION['errors'][] = "Apellidos Invalidos";
    }

    if (ValidarDni($_POST['dni'])){
        $_SESSION['errors'][] = "DNI Invalido";
    }
    if (ValidarTelefono($_POST['telefono'])){
        $_SESSION['errors'][] = "Telefono Invalido";
    }
    if (ValidarEmail($_POST['email'])){
        $_SESSION['errors'][] = "Email Invalido";
    }
    if (ValidarPassword($_POST['password'])){
        $_SESSION['errors'][] = "Password Invalido";
    }

    //if ($_SESSION['errors'].count() == 0) header("Location: ../views/welcome.php");
    header("Location: ../views/register.php");

}
?>


