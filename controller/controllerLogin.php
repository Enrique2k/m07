<?php

use Cliente;
require_once ('../helper/validations.php');
if (isset($_POST['submit']))
    if ($_POST['control'] == 'login'){
        $hash = getUserHash($_POST['dni']);
        if(password_verify($_POST['password'] , $hash)){
            session_start();
            $_SESSION['cliente'] = $_POST['dni'];
            header('Location: ../views/welcome.php');
        }
        else{
            require_once ('../views/login.php');
        }
    }


if ($_POST['password'] != null && $_POST['nombre'] != null ){
    $_SESSION['nombre'] = $_POST['nombre'];
    $_SESSION['password'] = $_POST['password'];
    header("Location: ../init.php");
}else {
    $_SESSION['error'] = 'Usuario o contraseña erroneos';
    header("Location: ../views/login.php");
}

?>


