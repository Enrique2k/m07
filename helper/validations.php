<?php

function validarNombre($nombre){

    if (!preg_match("/^[a-zA-Z'-]+$/",$nombre)){
        return false;
    }
    return true;
}
function validarApellidos($apellidos){

    if (!preg_match("/^[a-zA-Z'-]+$/",$apellidos)){
        return false;
    }
    return true;
}

function validarEdad($edad){
    if ($edad >= 18) return true;
    return false;
}

function validarDni($dni){
    $letra = substr($dni, -1);
    $numeros = substr($dni,0,-1);
    if (substr("TRWAGMYFPDXBNJZSQVHLCKE",$numeros%23,1) == $letra && strlen($letra) == 1 && strlen($numeros) == 8){
        return true;
    }
    else return false;
}

function validarTelefono($telefono){
    if ( ($telefono[0] == "6" || $telefono[0] == "7" ) && strlen($telefono) == 9)
        return true;
    else return false;

}

function validarEmail($email){
    if (filter_var($email,FILTER_VALIDATE_EMAIL))
        return true;
    else return false;
}

function validarPassword($password){
    if (validarPasswordLength($password) &&
        validarPasswordMayus($password) &&
        validarPasswordMinus($password) &&
        validarPasswordNum($password) &&
        validarPasswordCE($password)
    ) return true;
    else return false;

}

function validarPasswordLength($password){
    if (strlen($password) >=8 ) return true;
    else return false;
}

function validarPasswordMayus($password){
    for ($k = 0 ; $k < strlen($password); $k++){
        if (ctype_upper($password[$k]))
            return true;
    }
    return false;
}

function validarPasswordMinus($password){
    for ($k = 0 ; $k < strlen($password); $k++){
        if (ctype_lower($password[$k]))
            return true;
    }
    return false;
}

function validarPasswordNum($password){

    for ($k = 0 ; $k < strlen($password); $k++){
        if (ctype_digit($password[$k]))
            return true;
    }
    return false;
}

function validarPasswordCE($password){

    for ($k = 0 ; $k < strlen($password); $k++){
        if (!ctype_alpha($password[$k]) && !ctype_digit($password[$k]))
            return true;
    }
    return false;
}

function validateProfile(){

}

?>

