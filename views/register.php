<?php require_once('../helper/i18n.php');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo _("Registro");?></title>
</head>
<body>
<?php require_once('header.php');?>
<h1>
    <?php echo _("REGISTRO");?>
</h1>
<form action="init.php" method="post">
    <label for="nombre">Nombre</label>
    <input type="text" id="nombre" name="nombre">

    <label for ="apellidos">Apellidos</label>
    <input type="text" id="apellidos" name="apellidos">

    <label for ="sexo">Sexo</label>
    <select name="sexo" id="sexo">
        <option value = "0" >Elija su sexo</option>
        <option value = "1" >Hombre</option>
        <option value = "2" >Mujer</option>
    </select></br>

    <label for="dni">Dni</label>
    <input type="text" id="dni" name="dni">

    <label for="telefono">Telefono</label>
    <input type="tel" id="telefono" name="telefono">

    <label for="email">Email</label>
    <input type="email" id="email" name="email"></br>

    <label for="password">Password</label>
    <input type="password" id="password" name="password">
    <input name="control" value="login" type="hidden">
    <input type="submit" name="submit" value="submit">

</form>

<?php
session_start();
for ($x = 0; $x < $_SESSION['errors'].count() ; $x++){
    echo $_SESSION['errors'][$x];
}
?>

</body>
</html>



